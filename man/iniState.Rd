% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/iniState.R
\name{iniState}
\alias{iniState}
\title{Write initial state of the meta-population}
\usage{
iniState(parcels, diapause = FALSE, initMosq = 100000)
}
\arguments{
\item{parcels}{data.frame or data.table}

\item{diapause}{logical}

\item{initMosq}{numerical value Initial number of eggs in each node.}
}
\value{
list with two data.frame: u0 and v0

u0 is the initial population stage for each compartment in each parcel. Each row describe a patch and in columns:
 "Sh": susceptible humans ; "Eh": exposed humans ; "Ih": infectious humans ;"Rh": recovered humans ;
 "A1gmE", "A1omE", "A2hmE", "A2gmE", "A2omE": exposed stages for adult mosquitoes (see details of stages below)
 "A1gmI"  "A1omI"  "A2hmI"  "A2gmI"  "A2omI": infectious stages for adult mosquitoes (see details of stages below)
 "Neggs": number of eggs layed by infected mosquitoes
 "ninfhL": number of local human autochtonous infection
 "ninfhE": number of external human autochtonous infection
 "ninfm1": number of nulliparous mosquitoes autochtonous infection
 "ninfm2": number of parous mosquitoes autochtonous infection

v0 is the initial population and time-dependant parameters state. Each columns describe a patch and in rows:
z: diapause (0 = dipause, 1 = favorable period); temperature; kL and kP: carrying capacities for larvae and pupae (rainfall dependent);
Em: number of eggs ; Lm: number of larvae ; Pm: number of pupae ; Aemm: number of emerging adults ;
A1hm: number of nulliparous adults seeking for host ; A1gm: number of gorged nulliparous adults ; A1om: number of nulliparous adults seeking for oviposition sites ;
A2hm: number of parous adults seeking for host ; A2gm: number of gorged parous adults ; A1om: number of parous adults seeking for oviposition sites ;
prevEggs, nIm1, nIm2, R0, betaHext and betaMext are continous variables calculated over time)
newEggs: daily number of new layed eggs
}
\description{
initialize
}
\keyword{demography}
